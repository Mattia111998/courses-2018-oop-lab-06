package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {
	
	private static final int ELEMS = 100000;
	private static final int TO_MS = 1000000;
	private static final int INDEX_0 = 0;

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */

    public static void main(final String... s) {

        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	List<Integer> arrList = new ArrayList<>();
    	for(int i = 1000; i < 2000; i++) {
    		arrList.add(i);
    	}
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	List<Integer> lList = new LinkedList<>(arrList);
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	int index = arrList.size() - 1;
    	int temp = arrList.get(index); //LastElem
    	arrList.set(index, arrList.get(0)); //Swap LastElem
    	arrList.set(INDEX_0, temp); //Swap first elem
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	for(int i : arrList) {
    		System.out.println(i);
    	}
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	long time = System.nanoTime();
    	
    	for (int i = 1; i <= ELEMS; i++) {
    		arrList.add(INDEX_0, i);
        }
    	
    	time = System.nanoTime() - time;
        System.out.println("Inserting " + ELEMS + " in a ArrayList took " + time
                + "ns (" + time / TO_MS + "ms)");
        
        time = System.nanoTime();
        
        for (int i = 1; i <= ELEMS; i++) {
    		lList.add(INDEX_0, i);
        }
    	
    	time = System.nanoTime() - time;
    	System.out.println("Inserting " + ELEMS + " in a LinkedList took " + time
                + "ns (" + time / TO_MS + "ms)");
    	
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
    	
    	time = System.nanoTime();
    	int size = arrList.size() + 1;
    	
    	
    	for (int i = 1; i <= 1000; i++) {
    		arrList.get(size / 2);
        }
    	
    	time = System.nanoTime() - time;
        System.out.println("Reading 1000 elems in a ArrayList took " + time
                + "ns (" + time / TO_MS + "ms)");
        
        time = System.nanoTime();
        size = lList.size() + 1;
        
        for (int i = 1; i <= 1000; i++) {
    		lList.get(size / 2);
        }
    	
    	time = System.nanoTime() - time;
    	System.out.println("Reading 1000 elems in a LinkedList took " + time
                + "ns (" + time / TO_MS + "ms)");
    
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
    	Map<String, Long> global = new HashMap<>();
    	global.put("Africa", 1110635000l);
    	global.put("Americas", 972005000l);
    	global.put("Antartica", 0l);
    	global.put("Asia", 4298723000l);
    	global.put("Europe", 742452000l);
    	global.put("Oceania", 38304000l);
    	
        /*
         * 8) Compute the population of the world
         */
    	for(String key: global.keySet()) {
    		System.out.println("Continent: " + key + " population: " + global.get(key));
    	}
    	/*
    	for(Long value: global.values()) {
    		System.out.println("Population: " + value);
    	}
    	*/
    }
}
