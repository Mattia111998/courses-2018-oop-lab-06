/**
 * 
 */
package it.unibo.oop.lab.collections2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * Instructions
 * 
 * This will be an implementation of
 * {@link it.unibo.oop.lab.collections2.SocialNetworkUser}:
 * 
 * 1) complete the definition of the methods by following the suggestions
 * included in the comments below.
 * 
 * @param <U>
 *            Specific user type
 */
public class SocialNetworkUserImpl<U extends User> extends UserImpl implements SocialNetworkUser<U> {

    /*
     * 
     * [FIELDS]
     * 
     * Define any necessary field
     * 
     * In order to save the people followed by a user organized in groups, adopt
     * a generic-type Map:
     * 
     * think of what type of keys and values would best suit the requirements
     */
	final Map<String, List<U>> group = new HashMap<>(); //For group e.x = name group, utenti= pippo, paperino ecc

    /*
     * [CONSTRUCTORS]
     * 
     * 1) Complete the definition of the constructor below, for building a user
     * participating in a social network, with 4 parameters, initializing:
     * 
     * - firstName - lastName - username - age and every other necessary field
     * 
     * 2) Define a further constructor where age is defaulted to -1
     */
	
    /**
     * Builds a new {@link SocialNetworkUserImpl}.
     * 
     * @param name
     *            the user firstname
     * @param surname
     *            the user lastname
     * @param userAge
     *            user's age
     * @param user
     *            alias of the user, i.e. the way a user is identified on an
     *            application
     */
    public SocialNetworkUserImpl(final String name, final String surname, final String user, final int userAge) {
        super(name, surname, user, userAge);
    }
    
	public SocialNetworkUserImpl(final String name, final String surname, final String user) {
        this(name, surname, user, -1);
    }


    /*
     * [METHODS]
     * 
     * Implements the methods below
     */

    @Override
    public boolean addFollowedUser(final String circle, final U user) {
    	if(circle != "" && user != null) {
    		if(this.group.containsKey(circle)) { //If already exist group
    			for(final User usr: this.group.get(circle)) {
    				if(usr.equals(user)) { //If exist in group
    					return false;
    				}
    			}
    			this.group.get(circle).add(user);
    		} else { //If the group doesn't exist
    			this.group.put(circle, new ArrayList<>());
    			this.addFollowedUser(circle, user); //Call recorsive
    		}
    		return true;
    	}
    	return false;
    }

    @Override
    public Collection<U> getFollowedUsersInGroup(final String groupName) {
    	final List<U> followedUsersGroup = new ArrayList<>();
        if(this.group.containsKey(groupName)) {
        	//Difensive copy     //this.group.get(groupName)
        	followedUsersGroup.addAll((this.group.get(groupName)));
        }
        return followedUsersGroup;
    }

    @Override
    public List<U> getFollowedUsers() {
    	final List<U> finalUsers = new ArrayList<>();
    	if(!this.group.isEmpty()) {
	    	for(String key: this.group.keySet()) {
	    		final List<U> groupUsers = new ArrayList<>(this.group.get(key));
	    		for(final U usr: groupUsers) {
	    			if(!finalUsers.contains(usr)) {
	    				finalUsers.add(usr);
	    			}
	    		}
	    	}
    	}
    	return finalUsers;
    }

}
